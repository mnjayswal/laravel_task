
<?php
function checkfun($input_data)
{
    $input_data = trim($input_data);
    $input_data = stripslashes($input_data);
    $input_data = htmlspecialchars($input_data);
    return $input_data;
}
function errorshow(array $err):string
{
    $a = "";
    for ($i = 0; $i < count($err); $i++) {
        $a .= "<br>" . $err[$i];
    }
    return $a;
}
?>